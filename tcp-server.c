//Daniel Uribe & Shayn Garron
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <time.h>

#define SERVER_PORT 80
#define TCP_MSS_DEFAULT 536U    // IPv4 (RFC1122, RFC2581) 
#define INIT_SEQ 0x1337F001

typedef enum States { LISTEN, SYN_RECV, ESTABLISHED, CLOSE_WAIT, LAST_ACK, FIN_WAIT_1, FIN_WAIT_2 } state_t;

typedef struct packet {
  struct iphdr *ip; // points to the IP HDR portion of the payload
  struct tcphdr *tcp; // points to the TCP HDR portin of the payload
  
  char payload[IP_MAXPACKET]; // contains the IP, TCP and data
  int payload_size; // the total size of the payload, so you know how many bytes to write when sending.
  timer_t retransmit_timer;
} packet_t;



typedef struct tcpinfo {
  state_t state;
    
  uint32_t seq;
  uint32_t ack;

  uint16_t src_port;
  uint16_t dst_port;
  in_addr_t src_addr;
  in_addr_t dst_addr;

  uint32_t window;
  uint8_t window_scale;

  struct sockaddr* caddr;

  struct packet *first_unacked;
  struct packet *last_sent;

  int pfd;

  int sfd;
} tcp_info_t;

// could also pass tcpi structure, might be helpful...
void PrintPacketInfo(packet_t *packet) {
  printf("TCP Header Information:\n");
  printf("\tsrc port: %d\n", ntohs(packet->tcp->source));
  printf("\tdst port: %d\n", ntohs(packet->tcp->dest));
  
  // add more here for debugging purposes
}

// http://www.microhowto.info/howto/calculate_an_internet_protocol_checksum_in_c.html
uint16_t IPChecksum(void* vdata, size_t length) {
  // Cast the data pointer to one that can be indexed.
  char* data=(char*)vdata;

  // Initialise the accumulator.
  uint32_t acc=0xffff;

  // Handle complete 16-bit blocks.
  for (size_t i=0;i+1<length;i+=2) {
    uint16_t word;
    memcpy(&word,data+i,2);
    acc+=ntohs(word);
    if (acc>0xffff) {
      acc-=0xffff;
    }
  }

  // Handle any partial block at the end of the data.
  if (length&1) {
    uint16_t word=0;
    memcpy(&word,data+length-1,1);
    acc+=ntohs(word);
    if (acc>0xffff) {
      acc-=0xffff;
    }
  }

  // Return the checksum in network byte order.
  return htons(~acc);
}


uint16_t TCPChecksum (char *packet, struct iphdr *ip, struct tcphdr *tcp, int tcp_payload_len)
{
  char buf[IP_MAXPACKET];
  char *ptr;
  int chksumlen;

  ptr = &buf[0];
     
  struct pseudo_tcp_header
  {
    in_addr_t saddr, daddr;
    u_char reserved;
    u_char protocol;
    u_short tcp_size;
  } ps_head;

  uint16_t header_len = 4*tcp->doff;
     
  ps_head.saddr    = ip->saddr;
  ps_head.daddr    = ip->daddr;
  ps_head.reserved = 0;
  ps_head.protocol = ip->protocol;
  ps_head.tcp_size = htons(header_len + tcp_payload_len);
     
  memcpy(ptr, &ps_head, sizeof(struct pseudo_tcp_header));
  ptr += sizeof(struct pseudo_tcp_header);
     
  memcpy (ptr, tcp, header_len);  
  ptr += header_len;

     
  memcpy (ptr, packet + ip->ihl*4 + header_len, tcp_payload_len);
  ptr += tcp_payload_len;

  chksumlen = ptr - buf;
     
  return IPChecksum ((uint16_t *) buf, chksumlen);
}

void SendSynAck(packet_t *packet, tcp_info_t *tcpi, struct sockaddr *sin) {
  packet_t reply;
  memcpy(&reply.payload, packet->payload, packet->payload_size);
  reply.ip = (struct iphdr *)reply.payload;
  reply.tcp = (struct tcphdr *)(reply.payload + (4 * packet->ip->ihl));
  reply.payload_size = packet->payload_size;
  
  // swap the host/client addresses for TCP/IP
  reply.ip->saddr = packet->ip->daddr;
  reply.ip->daddr = packet->ip->saddr;
  reply.tcp->source = packet->tcp->dest;
  reply.tcp->dest = packet->tcp->source;

  // Increase ack seq by 1
  reply.tcp->ack_seq = htonl( ntohl(packet->tcp->seq)+1 );  
  // Send our initial sequence number (wireshark will show you the relative sequence number being 0
  // but we know that it is INIT_SEQ)
  reply.tcp->seq = htonl(INIT_SEQ);
  reply.tcp->ack = 1; // SYN-ACK, so ack is 1
  reply.tcp->check = 0;
  
  // save our next sequence number to the state
  tcpi->seq = INIT_SEQ+1;
  // save the ack-seq number
  tcpi->ack = ntohl(reply.tcp->ack_seq);

  reply.ip->check = IPChecksum( (uint16_t *) reply.payload, reply.ip->ihl * 4);
  reply.tcp->check = TCPChecksum(reply.payload, reply.ip, reply.tcp, packet->payload_size - 4*packet->ip->ihl - 4*packet->tcp->doff);

  if (sendto (tcpi->sfd, reply.payload, reply.payload_size, 0, sin, sizeof (struct sockaddr_in)) < 0) {
    fprintf(stderr, "sendto(): %s\n", strerror(errno));
    exit (1);
  }
}

// probably need a SendFin and SendFinAck for M3
// you will also needs functions for sending data 
void SendFin(packet_t *packet, tcp_info_t *tcpi, struct sockaddr *sin) {
packet_t reply;
  memcpy(&reply.payload, packet->payload, packet->payload_size);
  reply.ip = (struct iphdr *)reply.payload;
  reply.tcp = (struct tcphdr *)(reply.payload + (4 * packet->ip->ihl));
  reply.payload_size = packet->payload_size;
}


void SendFinAck(int sfd, char *buf, int payload_size, struct ip_hdr * ip, struct tcphdr *tcp, 
	struct sockaddr *sin, tcp_info_t *tcpi)
{	packet_t reply;
	memcpy(&reply.payload, packet->payload, packet->payload_size);
	
	struct iphdr  *reply.ip  = (struct iphdr*)  reply.payload;
	struct tcphdr *reply.tcp = (struct tcphdr*) (reply.payload + 4*ip->ihl);
	reply.ip = (struct iphdr*) reply.payload;
	reply.ip = (struct tcphdr*)(reply.payload + 4*packet->ip->ihl);
	reply.ip->daddr = ip->saddr;
	reply.ip->saddr = ip->daddr;
	reply.tcp->source = tcp->dest;
	reply.tcp->dest   = tcp->source;
    reply.tcp->ack_seq = htonl( ntohl(tcp->seq)+1 ); 
    reply.tcp->seq = tcp->ack_seq;
    reply.tcp->ack = 1;   
    reply.tcp->check = 0;
	//checksum
	reply.ip->check = checksum( (uint16_t *) reply.payload, 4 * ip->ihl);
    reply.tcp->check = TCPChecksum (reply.payload, reply.ip, reply.tcp, payload_size - 4*ip->ihl - 4*tcp->doff);
	
	//send
	if (sendto (sfd, reply.payload, payload_size, 0, 
		sin, sizeof (struct sockaddr_in)) < 0) {
			perror ("sendto(): %s\n");
			exit(1);
		}
}


void make_and_send_fin( int sfd, char *buf, int n, 
			struct iphdr *ip, struct tcphdr *tcp, 
			struct sockaddr *sin, tcp_info_t *tcpi)
{
	packet_t reply;
	memcpy(&reply.payload, packet->payload, packet->payload_size);
	
	struct iphdr  *r_ip  = (struct iphdr*)  reply.payload;
    struct tcphdr *r_tcp = (struct tcphdr*) (reply.payload + 4*ip->ihl);
    reply.ip->daddr = ip->saddr;
    reply.ip->saddr = ip->daddr;

    reply.tcp->source = tcp->dest;
    reply.tcp->dest   = tcp->source;
    reply.tcp->ack_seq = tcp->seq;
    reply.tcp->seq     = htonl( tcpi->seq );
    reply.tcp->ack = 0;
    reply.tcp->fin = 1;    
    reply.tcp->check = 0;
	
	reply.ip->check = checksum( (uint16_t *) reply.payload, 4 * ip->ihl);
    reply.tcp->check = TCPChecksum (reply.payload, reply.ip, reply.tcp, payload_size - 4*ip->ihl - 4*tcp->doff );
	
	if (sendto ( sfd, reply.payload, payload_size, 0,
		sin, sizeof (struct sockaddr_in)) < 0)
		{
			perror ("sendto(): %s\n");
			exit(1);
		}
}			
void SendPayloadAck( int sfd, char *buf, int n, char *payload, 
					 int payload_s, struct iphdr *ip, struct tcphdr *tcp, 
					 struct sockaddr *sin, tcp_info_t *tcpi)
{
	packet_t reply;
	memcpy(reply.payload, buf, payload_size);
	memcpy(reply.payload + payload_size, payload, payload_s);
	
	struct iphdr  *reply.ip  = (struct iphdr*)  reply.payload;
    struct tcphdr *reply.tcp = (struct tcphdr*) (reply.payload + 4*ip->ihl);
	reply.ip->daddr = ip->saddr;
    reply.ip->saddr = ip->daddr;

    reply.ip->tcp_payload_len += payload_s;
	reply.tcp->source = tcp->dest;
    reply.tcp->dest   = tcp->source;
    reply.tcp->ack_seq = tcp->seq;
    reply.tcp->seq     = htonl( tcpi>seq );
    reply.tcp->ack = 1;    
    reply.tcp->check = 0;
	
	tcpi->seq += payload_s;
    tcpi->ack =  ntohl( tcp->ack_seq );
	
	reply.ip->check = checksum( (uint16_t *) reply.payload, 4 * ip->ihl);
    reply.tcp->check = tcp4_checksum (reply.payload, reply.ip, reply.tcp, payload_size - 4*ip->ihl - 4*tcp->doff + payload_s);
	
	if (sendto (sfd, reply.payload, n+payload_s, 0,
		    sin, sizeof (struct sockaddr_in)) < 0)  
			{
				perror ("sendto(): %s\n");
				exit (1);
			}
	 
}

int getWindowScale( packet_t *pkt, int *window )
{
     uint8_t *opt = (uint8_t*)pkt->tcp + sizeof(struct tcphdr);
     int found = 0;
     while( *opt != 0 ) {
	  tcp_option_t* _opt = (tcp_option_t*)opt;
	  if( _opt->kind == TCPOPT_NOP ) {
	       ++opt;  
	       continue;
	  }
	  if( _opt->kind == TCPOPT_WINDOW ) {
	       *window = *(opt + 2);
	       found = 1;
	       break;
	  }
	  opt += _opt->size;
     }
     return found;
}

void updateWindow(packet_t *pkt, tcp_info_t *tcpi)
{
     int window_scale;
     int old_window = tcpi->window
     if( tcpi.state = SYN_RECV) ){
	  if( getWindowScale(pkt,window_scale) ){
	       tcpi->window_scale = window_scale;
	       tcpi->window       = ntohs(pkt->tcp->window) << window_scale;
	       fprintf(stderr, "Got window scaling factor %d, window size %d\n",
		       tcpi->window_scale, tcpi->window );
	  }
	  else{
	       tcpi->window_scale = 0;
	       tcpi->window= ntohs(pkt->tcp->window);
	       fprintf(stderr, "No window scaling opting in SYN, window size %d\n", tcb->window );
	  }
     }
     else { // presumes this is an ACK
	  if( tcpi->window_scale > 0 )
	       tcpi->window = ntohs(pkt->tcp->window) << tcpi->window_scale;
	  else
	       tcpi->window = ntohs(pkt->tcp->window);
     }
     if( old_window != tcpi->window )
	  fprintf(stderr, "WINDOW: got new window %d (tcp.window %d)\n",
		  tcb->window, ntohs(pkt->tcp->window) ); 
}

int main(int argc, char *argv[]) {
 
  // socket file descriptor
  int sfd;
  // length of client addr
  socklen_t clen = sizeof(struct sockaddr_in);
  // client addr
  packet_t incoming_packet;

  // Create a RAW Socket for TCP
  sfd = socket(AF_INET, SOCK_RAW, IPPROTO_TCP);
  if(sfd < 0) {
    fprintf(stderr, "socket() error: %s\n", strerror(errno));
    exit(1);
  }

  // See 28.2 in UNIX Network Programming Vol 1 3rd Ed.
  // Ensures we will be using IPv4 and get the headers
  const int on = 1;
  int res = setsockopt(sfd, IPPROTO_IP, IP_HDRINCL, &on, sizeof(on));
  if(res < 0) {
    fprintf(stderr, "setsockopt() error: %s\n", strerror(errno));
    exit(1);
  }

  // just one since we are only going to handle one connection.
  tcp_info_t tcpi;
  tcpi.state = LISTEN;
  tcpi.sfd = sfd;
  struct sockaddr_in cliaddr;
  
  // TCP state machine!
  while(1) {
    
    // Recv a packet from the socket into buffer with a max size of IP_MAXPACKET, no flags, store the client address in the caddr structure
    incoming_packet.payload_size = recvfrom(sfd, incoming_packet.payload, IP_MAXPACKET, 0, (struct sockaddr *) &cliaddr, &clen);

    // Recall the idea of encapsulation
    // The data portion of the IP header contains TCP and any other layers above that.
    incoming_packet.ip = (struct iphdr*)incoming_packet.payload;
    // wtf (what the fI love coding in C) is going on here?
    // Our socket receives an IP packet, so technically the TCP information is part of that data (encapsulation!)
    // Where does it start?
    // Well the ADDRESS of the ip_hdr (the beginning of the ip_hdr in memory) + the length of the header
    // But wait, why times 4?
    // Because the header is given in 32 bit words (aka 4 bytes).
    // Buffer is a character so the addressing is in bytes. So we can pick the address that is the beginning of the ip_hdr + the header length * 4, that gives us the data portion of the ip_hdr which is...?
    // THE BEGINNING OF THE TCP HEADER, cool.
    incoming_packet.tcp = (struct tcphdr *)(incoming_packet.payload + (4 * incoming_packet.ip->ihl));

    if(ntohs(incoming_packet.tcp->dest) == SERVER_PORT) {
      PrintPacketInfo(&incoming_packet);
      fprintf(stderr, "State: %d\n", tcpi.state);

      switch(tcpi.state) {
      //LISTEN, SYN_RECV, ESTABLISHED, CLOSE_WAIT, LAST_ACK, FIN_WAIT_1, FIN_WAIT_2
      case LISTEN:
            // SYN received 
            if(incoming_packet.tcp->syn & !incoming_packet.tcp->ack) {
              SendSynAck(&incoming_packet, &tcpi, (struct sockaddr *)&cliaddr);
              tcpi.state = SYN_RECV;
               
               updateWindow(incoming_packet,tcpi)
            //update sliding window
            }
            break;
      case SYN_RECV:
	  
			if(incoming_packet.tcp->ack && tcpi.state == SYN_RECV && incoming_packet.tcp->ack == INIT_SEQ+1 ){
				printf( "Handshake.\n");
            // NO BREAK NEEDED
            // Fall through. 
            tcpi.state = ESTABLISHED;
            //if payload start fill window with payloads, set flag to true

      case ESTABLISHED:
            //check if ack matches
            if(incoming_packet.tcp->ack && tcpi.state == SYN_RECV && incoming_packet.tcp->ack == INIT_SEQ+1)
                printf("Handshake success\n");
            // send your payload
            SendPayloadAck( sfd, incoming_packet, incoming_packet.payload_size, incoming_packet->payload, 
					incoming_packet.payload_size, ip, incoming_packet.tcp, 
					(struct sockaddr *)&cliaddr, tcpi)
{
            // When finished or starting on M3, send your FIN packet!
            make_and_send_fin( sfd, incoming_packet->payload, incoming_packet.payload_size, 
			        *ip, *tcp, (struct sockaddr *)&cliaddr, t*tcpi)

            updateWindow(incoming_packet,tcpi);
            tcpi.state = FIN_WAIT_1;
            break;
      case FIN_WAIT_1:
      
            SendFinAck(sfd, incoming_packet, incoming_packet.payload_size, incoming_packet->payload, 
			incoming_packet.payload_size, ip, incoming_packet.tcp);
            

            make_and_send_fin(sfd, incoming_packet, incoming_packet.payload_size, incoming_packet->payload, 
			incoming_packet.payload_size, ip, incoming_packet.tcp)

            tcpi.state = FIN_WAIT_2;
            break;
      case FIN_WAIT_2:
      
            SendFinAck(sfd, incoming_packet, incoming_packet.payload_size, incoming_packet->payload, 
			incoming_packet.payload_size, ip, incoming_packet.tcp);

            make_and_send_fin(sfd, incoming_packet, incoming_packet.payload_size, incoming_packet->payload, 
			incoming_packet.payload_size, ip, incoming_packet.tcp)

            tcpi.state = CLOSE_WAIT;
            break;
      case CLOSE_WAIT:
      
            tcpi.state = LAST_ACK;
            break;
      case LAST_ACK:
            
            tcpi.state = LISTEN;
            
            break;
      }
    }
  }
  return 0;
}
